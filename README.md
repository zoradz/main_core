# README #

Last development for the JS-Core.

Core logging & buffer functionality for RuneJS applications

### What is this repository for? ###

RuneLogger singleton Pino logger wrapper:
logger.info(...messages)
logger.debug(...messages)
logger.warn(...messages)
logger.error(...messages)
logger.fatal(...messages)
logger.trace(...messages)
Ability to set the Pino logging date/time formatting function via setLoggerTimeFn(Pino.TimeFn)
Ability to set the Pino logging pretty print config value via setLoggerPrettyPrint(boolean)



Tyrannus & TheBlackParade